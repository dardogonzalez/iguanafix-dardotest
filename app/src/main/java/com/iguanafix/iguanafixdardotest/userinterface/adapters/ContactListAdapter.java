package com.iguanafix.iguanafixdardotest.userinterface.adapters;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.text.Html;
import android.text.Spanned;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.iguanafix.iguanafixdardotest.R;
import com.iguanafix.iguanafixdardotest.helpers.ImageDownloader;
import com.iguanafix.iguanafixdardotest.models.ContactDetail;
import com.iguanafix.iguanafixdardotest.models.Phone;

import java.util.ArrayList;
import java.util.List;

public class ContactListAdapter extends RecyclerView.Adapter<ContactListAdapter.ContactViewHolder> {

    private static final String HTML_COLOR_START = "<font color=\"#FF0000\">";
    private static final String HTML_COLOR_END = "</font>";
    private Context context;
    private String isSearchingWhat;
    private OnContactClickedListener onContactClickedListener;
    private List<ContactDetail> contactDetails = new ArrayList<>();

    public ContactListAdapter(Context context, OnContactClickedListener onContactClickedListener) {
        this.context = context;
        this.onContactClickedListener = onContactClickedListener;
    }

    public void clearContactDetails() {
        updateContactDetails(new ArrayList<ContactDetail>());
    }

    public void updateContactDetails(List<ContactDetail> contactDetails) {
        updateContactDetails(contactDetails, null);
    }

    public void updateContactDetails(List<ContactDetail> contactDetails, String isSearchingWhat) {
        this.contactDetails = contactDetails;
        this.isSearchingWhat = isSearchingWhat;
        notifyDataSetChanged();
    }

    @Override
    public ContactViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.fragment_contact_list_item, parent, false);
        return new ContactViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(ContactViewHolder holder, int position) {
        final ContactDetail contactDetail = contactDetails.get(position);
        holder.indexTextView.setText(String.valueOf(contactDetail.getId()));
        holder.nameTextView.setText(formatStringIfSearching(contactDetail.getFirstName() + " " + contactDetail.getLastName()));
        holder.cellPhoneTextView.setText(getPhoneByType(contactDetail, Phone.Types.Cellphone));
        holder.officePhoneTextView.setText(getPhoneByType(contactDetail, Phone.Types.Office));
        holder.homePhoneTextView.setText(getPhoneByType(contactDetail, Phone.Types.Home));
        ImageDownloader.getInstance().retrieveImage(contactDetail.getThumb(), holder.avatarImageView, R.mipmap.default_contact_photo);

        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(onContactClickedListener != null) {
                    onContactClickedListener.onContactClicked(contactDetail);
                }
                notifyDataSetChanged();
            }
        });
    }

    private Spanned getPhoneByType(ContactDetail contactDetail, Phone.Types type) {
        Phone phone = contactDetail.getPhoneByType(type);
        if(phone != null && phone.getNumber() != null) {
            return formatStringIfSearching(phone.getNumber());
        }
        return formatStringIfSearching(context.getString(R.string.no_phone));
    }

    private Spanned formatStringIfSearching(String text) {
        if (!TextUtils.isEmpty(isSearchingWhat)) {
            int index = text.toLowerCase().indexOf(isSearchingWhat);
            if(index >= 0) {
                text = formatString(text, index, isSearchingWhat.length());
            }
        }
        return Html.fromHtml(text);
    }

    private String formatString(String text, int index, int length) {
        StringBuilder builder = new StringBuilder();
        builder.append(text.substring(0, index));
        builder.append(HTML_COLOR_START);
        builder.append(text.substring(index, index + length));
        builder.append(HTML_COLOR_END);
        builder.append(text.substring(index + length));
        return builder.toString();
    }

    @Override
    public int getItemCount() {
        return contactDetails.size();
    }

    public class ContactViewHolder extends RecyclerView.ViewHolder {
        private ImageView avatarImageView;
        public TextView nameTextView;
        public TextView cellPhoneTextView;
        public TextView officePhoneTextView;
        public TextView indexTextView;
        public TextView homePhoneTextView;

        public ContactViewHolder(View view) {
            super(view);
            avatarImageView = view.findViewById(R.id.avatarImageView);
            nameTextView = view.findViewById(R.id.nameTextView);
            indexTextView = view.findViewById(R.id.indexTextView);
            cellPhoneTextView = view.findViewById(R.id.cellPhoneTextView);
            officePhoneTextView = view.findViewById(R.id.officePhoneTextView);
            homePhoneTextView = view.findViewById(R.id.homePhoneTextView);
        }
    }

    public interface OnContactClickedListener {
        void onContactClicked(ContactDetail contactDetail);
    }
}
