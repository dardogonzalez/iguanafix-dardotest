package com.iguanafix.iguanafixdardotest.userinterface.activities;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;

import com.f2prateek.dart.Dart;
import com.f2prateek.dart.InjectExtra;
import com.iguanafix.iguanafixdardotest.R;
import com.iguanafix.iguanafixdardotest.models.ContactDetail;
import com.iguanafix.iguanafixdardotest.userinterface.fragments.ContactDetailsFragment;

import butterknife.BindView;
import butterknife.ButterKnife;

public class ContactDetailsActivity extends AppCompatActivity{

    private ContactDetailsFragment contactDetailsFragment;

    @BindView(R.id.toolbar) Toolbar toolbar;
    @InjectExtra ContactDetail contactDetail;

    public static Intent createStartIntent(Context context, ContactDetail contactDetail) {
        return Henson.with(context).gotoContactDetailsActivity().contactDetail(contactDetail).build();
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Dart.inject(this);
        setContentView(R.layout.activity_contact_details);
        ButterKnife.bind(this);

        setupActionBar();
        setupFragment();
        if(savedInstanceState == null) {
            contactDetailsFragment.setContactDetails(contactDetail);
        }
    }

    private void setupActionBar() {
        setSupportActionBar(toolbar);
        ActionBar actionBar = getSupportActionBar();
        actionBar.setDisplayHomeAsUpEnabled(true);
        actionBar.setTitle(R.string.contact_details_activity_action_bar_title);
        toolbar.setNavigationOnClickListener(backNavigationOnClickListener);
    }

    private void setupFragment() {
        contactDetailsFragment = (ContactDetailsFragment) getSupportFragmentManager().findFragmentById(R.id.contact_details_fragment);
        contactDetailsFragment.forceLandscape();
    }

    private View.OnClickListener backNavigationOnClickListener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            onBackPressed();
        }
    };
}
