package com.iguanafix.iguanafixdardotest.userinterface.viewmodels;

import android.app.Application;
import android.arch.lifecycle.AndroidViewModel;
import android.arch.lifecycle.MutableLiveData;
import android.graphics.Bitmap;
import android.graphics.drawable.Drawable;
import android.os.AsyncTask;
import android.support.annotation.NonNull;
import android.text.TextUtils;
import android.widget.Toast;

import com.iguanafix.iguanafixdardotest.R;
import com.iguanafix.iguanafixdardotest.helpers.BitmapHelper;
import com.iguanafix.iguanafixdardotest.helpers.ImageDownloader;
import com.iguanafix.iguanafixdardotest.helpers.LogHelper;
import com.iguanafix.iguanafixdardotest.managers.ContactsManager;
import com.iguanafix.iguanafixdardotest.models.ContactDetail;
import com.iguanafix.iguanafixdardotest.models.Phone;
import com.iguanafix.iguanafixdardotest.rest.support.DisposableRequest;
import com.iguanafix.iguanafixdardotest.rest.support.OnItemRequestCompleted;
import com.iguanafix.iguanafixdardotest.rest.support.OnListRequestCompleted;
import com.squareup.picasso.Picasso;
import com.squareup.picasso.Target;

import java.util.ArrayList;
import java.util.List;

public class ContactListActivityViewModel extends AndroidViewModel {

    private Bitmap photoBitmap;
    private ContactsManager contactsManager;
    private DisposableRequest ongoingRequest;
    private SearchAsyncTask searchAsyncTaskRunning;
    private MutableLiveData<ContactDetail> contactDetailObservable = new MutableLiveData<>();
    private MutableLiveData<Bitmap> contactDetailPhotoObservable = new MutableLiveData<>();
    private ArrayList<ContactDetail> contactDetails = new ArrayList<>();
    private MutableLiveData<List<ContactDetail>> contactDetailsObservable = new MutableLiveData<>();
    private String photoLoading;

    public MutableLiveData<Bitmap> getContactDetailPhotoObservable() {
        return contactDetailPhotoObservable;
    }

    public MutableLiveData<ContactDetail> getContactDetailObservable() {
        return contactDetailObservable;
    }

    public MutableLiveData<List<ContactDetail>> getContactDetailsObservable() {
        return contactDetailsObservable;
    }

    public ContactListActivityViewModel(@NonNull Application application) {
        super(application);
        contactsManager = ContactsManager.getInstance();
    }

    public void dispose() {
        if (ongoingRequest != null) {
            ongoingRequest.dispose();
            ongoingRequest = null;
        }
        if(searchAsyncTaskRunning != null) {
            searchAsyncTaskRunning.cancel(true);
            searchAsyncTaskRunning = null;
        }
    }

    public void retrieveContactDetails() {
        ongoingRequest = contactsManager.getContacts(onGetContactsCompleted);
    }

    private OnListRequestCompleted<ContactDetail> onGetContactsCompleted = new OnListRequestCompleted<ContactDetail>() {
        @Override
        public void onSuccess(ArrayList<ContactDetail> contactDetails) {
            ongoingRequest = null;
            ContactListActivityViewModel.this.contactDetails = contactDetails;
            contactDetailsObservable.setValue(contactDetails);
        }

        @Override
        public void onError(Throwable throwable) {
            ongoingRequest = null;
            ContactListActivityViewModel.this.contactDetails = new ArrayList<>();
            contactDetailsObservable.setValue(null);
        }
    };

    public void clearContactDetail() {
        photoBitmap = null;
        contactDetailObservable.setValue(null);
    }

    public void retrieveFullContactDetail(ContactDetail contactDetail) {
        contactDetailObservable.setValue(contactDetail);
        retrieveContactDetail(contactDetail.getUserId());
    }

    public void retrieveContactDetail(String userId) {
        photoBitmap = null;
        ongoingRequest = ContactsManager.getInstance().getContact(userId, onGetContactCompleted);
    }

    private OnItemRequestCompleted<ContactDetail> onGetContactCompleted = new OnItemRequestCompleted<ContactDetail>() {
        @Override
        public void onSuccess(ContactDetail contactDetail) {
            ongoingRequest = null;
            contactDetailObservable.setValue(contactDetail);
            loadPhoto(contactDetail.getPhoto());
        }

        @Override
        public void onError(Throwable throwable) {
            ongoingRequest = null;
        }
    };

    public void clearSearch() {
        doSearch(null);
    }

    public void doSearch(String what) {
        if(searchAsyncTaskRunning != null) {
            searchAsyncTaskRunning.cancel(true);
        }
        searchAsyncTaskRunning = new SearchAsyncTask();
        searchAsyncTaskRunning.execute(what);
    }

    private void loadPhoto(String photo) {
        if (photoBitmap != null) {
            contactDetailPhotoObservable.postValue(photoBitmap);
            return;
        }
        LogHelper.debug(this, "Requesting image from server: " + photo);
        photoLoading = photo;
        ImageDownloader.getInstance().retrieveImage(photo, target);
    }

    private Target target = new Target() {
        @Override
        public void onBitmapLoaded(Bitmap bitmap, Picasso.LoadedFrom from) {
            try {
                if(photoLoading != null && photoLoading.equals(contactDetailObservable.getValue().getPhoto())) {
                    photoBitmap = BitmapHelper.resizeAndCompress(300, bitmap);
                    contactDetailPhotoObservable.postValue(photoBitmap);
                }
            } catch (Exception ex) {
                LogHelper.exception(ex);
            }
        }

        @Override
        public void onBitmapFailed(Drawable errorDrawable) {
            contactDetailPhotoObservable.postValue(null);
        }

        @Override
        public void onPrepareLoad(Drawable placeHolderDrawable) {
        }
    };

    private class SearchAsyncTask extends AsyncTask<String, Integer, ArrayList<ContactDetail>> {
        private String isSearchingWhat;

        @Override
        protected ArrayList<ContactDetail> doInBackground(String... strings) {
            isSearchingWhat = strings[0];
            if(TextUtils.isEmpty(isSearchingWhat)) {
                return contactDetails;
            }
            return doSearchAsync();
        }

        @Override
        protected void onPostExecute(ArrayList<ContactDetail> contactDetails) {
            searchAsyncTaskRunning = null;
            contactDetailsObservable.postValue(contactDetails);
        }

        private ArrayList<ContactDetail> doSearchAsync() {
            ArrayList<ContactDetail> filteredContactDetails = new ArrayList<>();
            for(ContactDetail contactDetail : contactDetails) {
                if(hasToInclude(contactDetail)) {
                    filteredContactDetails.add(contactDetail);
                }
            }
            return filteredContactDetails;
        }

        private boolean hasToInclude(ContactDetail contactDetail) {
            String lowerCaseWhat = isSearchingWhat.toLowerCase();
            if(contactDetail.getFirstName().toLowerCase().contains(lowerCaseWhat) ||
                    contactDetail.getLastName().toLowerCase().contains(lowerCaseWhat)) {
                return true;
            }
            for(Phone phone : contactDetail.getPhones()) {
                if(!TextUtils.isEmpty(phone.getNumber()) && phone.getNumber().contains(lowerCaseWhat)) {
                    return true;
                }
            }
            return false;
        }
    }
}
