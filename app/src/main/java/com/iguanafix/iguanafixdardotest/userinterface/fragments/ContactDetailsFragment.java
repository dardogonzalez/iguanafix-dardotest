package com.iguanafix.iguanafixdardotest.userinterface.fragments;

import android.arch.lifecycle.Observer;
import android.arch.lifecycle.ViewModelProviders;
import android.graphics.Bitmap;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.annotation.StringRes;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.iguanafix.iguanafixdardotest.R;
import com.iguanafix.iguanafixdardotest.helpers.ImageDownloader;
import com.iguanafix.iguanafixdardotest.helpers.BitmapHelper;
import com.iguanafix.iguanafixdardotest.helpers.LogHelper;
import com.iguanafix.iguanafixdardotest.managers.ContactsManager;
import com.iguanafix.iguanafixdardotest.models.ContactDetail;
import com.iguanafix.iguanafixdardotest.models.Phone;
import com.iguanafix.iguanafixdardotest.rest.support.DisposableRequest;
import com.iguanafix.iguanafixdardotest.rest.support.OnItemRequestCompleted;
import com.iguanafix.iguanafixdardotest.userinterface.viewmodels.ContactListActivityViewModel;
import com.squareup.picasso.Picasso;
import com.squareup.picasso.Target;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

import static android.content.res.Configuration.ORIENTATION_LANDSCAPE;

public class ContactDetailsFragment extends Fragment {

    private View fragmentView;
    private ContactListActivityViewModel viewModel;
    private boolean forceLandscape;

    @BindView(R.id.nameTextView) TextView nameTextView;
    @BindView(R.id.photoImageView) ImageView photoImageView;
    @BindView(R.id.phonesTitelTextView) TextView phonesTitelTextView;
    @BindView(R.id.phonesLinearLayout) LinearLayout phonesLinearLayout;
    @BindView(R.id.workAddressTitelTextView) TextView workAddressTitelTextView;
    @BindView(R.id.workAddressLinearLayout) LinearLayout workAddressLinearLayout;
    @BindView(R.id.homeAddressTitelTextView) TextView homeAddressTitelTextView;
    @BindView(R.id.homeAddressLinearLayout) LinearLayout homeAddressLinearLayout;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        fragmentView = selectProperContentView(inflater, container);
        ButterKnife.bind(this, fragmentView);
        return fragmentView;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        view.setVisibility(View.GONE);
        viewModel = ViewModelProviders.of(getActivity()).get(ContactListActivityViewModel.class);
        viewModel.getContactDetailObservable().observe(this, contactDetailObserver);
        viewModel.getContactDetailPhotoObservable().observe(this, contactDetailPhotoObserver);
    }

    private View selectProperContentView(LayoutInflater inflater, ViewGroup container) {
        boolean isTablet = getResources().getBoolean(R.bool.is_tablet);
        boolean isLandscape = getResources().getConfiguration().orientation == ORIENTATION_LANDSCAPE;
        if(isTablet && isLandscape && !forceLandscape) {
            // Para una tablet en landscape este fragment se debería ver como si fuera en portrait
            return inflater.inflate(R.layout.fragment_contact_details_portrait, container, false);
        }
        return inflater.inflate(R.layout.fragment_contact_details, container, false);
    }

    public void forceLandscape() {
        this.forceLandscape = true;
    }

    private Observer<ContactDetail> contactDetailObserver = new Observer<ContactDetail>() {
        @Override
        public void onChanged(@Nullable ContactDetail contactDetails) {
            loadViewWithContactDetail(contactDetails);
        }
    };

    private Observer<Bitmap> contactDetailPhotoObserver = new Observer<Bitmap>() {
        @Override
        public void onChanged(@Nullable Bitmap bitmap) {
            if(bitmap != null) {
                photoImageView.setImageBitmap(bitmap);
            } else {
                try { Toast.makeText(getContext(), R.string.cannot_load_photo, Toast.LENGTH_SHORT).show(); }
                catch (Exception ex) { LogHelper.exception(ex); }
            }
        }
    };

    public void setContactDetails(ContactDetail contactDetail) {
        loadViewWithContactDetail(contactDetail);
        viewModel.retrieveContactDetail(contactDetail.getUserId());
    }

    private void loadViewWithContactDetail(ContactDetail contactDetail) {
        if(contactDetail == null) {
            getView().setVisibility(View.GONE);
            return;
        }
        getView().setVisibility(View.VISIBLE);
        photoImageView.setImageResource(R.mipmap.default_contact_photo);
        nameTextView.setText(contactDetail.getFirstName() + " " + contactDetail.getLastName());
        loadPhones(contactDetail);
        loadHomeAddress(contactDetail);
        loadWorkAddress(contactDetail);
    }

    private void loadHomeAddress(ContactDetail contactDetail) {
        homeAddressLinearLayout.removeAllViews();
        for(String address : contactDetail.getHomeAddresses()) {
            addComponent(R.string.empty_string, address, homeAddressLinearLayout);
        }
        homeAddressTitelTextView.setVisibility(homeAddressLinearLayout.getChildCount() == 0 ? View.GONE : View.VISIBLE);
    }

    private void loadWorkAddress(ContactDetail contactDetail) {
        workAddressLinearLayout.removeAllViews();
        for(String address : contactDetail.getWorkAddresses()) {
            addComponent(R.string.empty_string, address, workAddressLinearLayout);
        }
        workAddressTitelTextView.setVisibility(workAddressLinearLayout.getChildCount() == 0 ? View.GONE : View.VISIBLE);
    }

    private void loadPhones(ContactDetail contactDetail) {
        phonesLinearLayout.removeAllViews();
        Phone phone = contactDetail.getPhoneByType(Phone.Types.Cellphone);
        if(phone != null && phone.getNumber() != null) {
            addComponent(R.string.cell_phone, phone.getNumber(), phonesLinearLayout);
        }
        phone = contactDetail.getPhoneByType(Phone.Types.Office);
        if(phone != null && phone.getNumber() != null) {
            addComponent(R.string.office_phone, phone.getNumber(), phonesLinearLayout);
        }
        phone = contactDetail.getPhoneByType(Phone.Types.Home);
        if(phone != null && phone.getNumber() != null) {
            addComponent(R.string.home_phone, phone.getNumber(), phonesLinearLayout);
        }
        phonesTitelTextView.setVisibility(phonesLinearLayout.getChildCount() == 0 ? View.GONE : View.VISIBLE);
    }

    private void addComponent(@StringRes int titleResourceId, String value, LinearLayout container) {
        LayoutInflater inflater = getActivity().getLayoutInflater();
        View view = inflater.inflate(R.layout.fragment_contact_detail_info_data, null, false);
        TextView titleTextView = view.findViewById(R.id.titleTextView);
        TextView descriptionTextView = view.findViewById(R.id.descriptionTextView);
        if(titleResourceId == R.string.empty_string) {
            titleTextView.setVisibility(View.GONE);
        } else {
            titleTextView.setText(titleResourceId);
        }
        descriptionTextView.setText(value);
        container.addView(view);
    }
}
