package com.iguanafix.iguanafixdardotest.userinterface.fragments;

import android.arch.lifecycle.Observer;
import android.arch.lifecycle.ViewModelProviders;
import android.content.Context;
import android.os.AsyncTask;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.os.Bundle;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;

import com.iguanafix.iguanafixdardotest.IguanaFixDardoTestApplication;
import com.iguanafix.iguanafixdardotest.R;
import com.iguanafix.iguanafixdardotest.helpers.LogHelper;
import com.iguanafix.iguanafixdardotest.managers.ContactsManager;
import com.iguanafix.iguanafixdardotest.models.ContactDetail;
import com.iguanafix.iguanafixdardotest.models.Phone;
import com.iguanafix.iguanafixdardotest.rest.support.DisposableRequest;
import com.iguanafix.iguanafixdardotest.rest.support.OnListRequestCompleted;
import com.iguanafix.iguanafixdardotest.userinterface.adapters.ContactListAdapter;
import com.iguanafix.iguanafixdardotest.userinterface.viewmodels.ContactListActivityViewModel;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

public class ContactListFragment extends Fragment {

    private View fragmentView;
    private OnContactSelectedListener contactSelectedListener;
    private ContactListAdapter contactListAdapter;
    private ContactListActivityViewModel viewModel;
    private String isSearchingWhat;

    @BindView(R.id.recyclerView) RecyclerView recyclerView;
    @BindView(R.id.errorLinearLayout) LinearLayout errorLinearLayout;
    @BindView(R.id.swipeRefreshLayout) SwipeRefreshLayout swipeRefreshLayout;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        fragmentView = inflater.inflate(R.layout.fragment_contact_list, container, false);
        ButterKnife.bind(this, fragmentView);
        setupView();
        return fragmentView;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        setupViewModel();
    }

    private void setupView() {
        swipeRefreshLayout.setOnRefreshListener(swipeRefreshLayoutOnRefreshListener);
        contactListAdapter = new ContactListAdapter(getContext(), onContactClickedListener);
        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(IguanaFixDardoTestApplication.getAppContext());
        recyclerView.setLayoutManager(mLayoutManager);
        recyclerView.setItemAnimator(new DefaultItemAnimator());
        recyclerView.setAdapter(contactListAdapter);
        swipeRefreshLayout.setRefreshing(true);
    }

    private void setupViewModel() {
        viewModel = ViewModelProviders.of(getActivity()).get(ContactListActivityViewModel.class);
        viewModel.getContactDetailsObservable().observe(this, contactDetailsObserver);
        viewModel.retrieveContactDetails();
    }

    private Observer<List<ContactDetail>> contactDetailsObserver = new Observer<List<ContactDetail>>() {
        @Override
        public void onChanged(@Nullable List<ContactDetail> contactDetails) {
            swipeRefreshLayout.setRefreshing(false);
            if(contactDetails != null) {
                errorLinearLayout.setVisibility(View.GONE);
                contactListAdapter.updateContactDetails(contactDetails, isSearchingWhat);
            } else {
                errorLinearLayout.setVisibility(View.VISIBLE);
                contactListAdapter.clearContactDetails();
            }
        }
    };

    public void clearSearch() {
        isSearchingWhat = null;
        viewModel.clearSearch();
    }

    public void doSearch(String what) {
        isSearchingWhat = what;
        viewModel.doSearch(what);
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        try { contactSelectedListener = (OnContactSelectedListener) context;}
        catch (ClassCastException e) {throw new ClassCastException(context.toString() + " must implement OnContactSelectedListener");}
    }

    private SwipeRefreshLayout.OnRefreshListener swipeRefreshLayoutOnRefreshListener = new SwipeRefreshLayout.OnRefreshListener() {
        @Override
        public void onRefresh() {
            viewModel.retrieveContactDetails();
        }
    };

    private ContactListAdapter.OnContactClickedListener onContactClickedListener = new ContactListAdapter.OnContactClickedListener() {
        @Override
        public void onContactClicked(ContactDetail contactDetail) {
            viewModel.retrieveFullContactDetail(contactDetail);
            contactSelectedListener.OnContactSelected(contactDetail);
        }
    };

    public interface OnContactSelectedListener {
        void OnContactSelected(ContactDetail contactDetail);
    }



}
