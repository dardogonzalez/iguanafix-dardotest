package com.iguanafix.iguanafixdardotest.userinterface.activities;

import android.arch.lifecycle.ViewModelProviders;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.text.Editable;
import android.view.KeyEvent;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.inputmethod.EditorInfo;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.iguanafix.iguanafixdardotest.R;
import com.iguanafix.iguanafixdardotest.models.ContactDetail;
import com.iguanafix.iguanafixdardotest.userinterface.fragments.ContactDetailsFragment;
import com.iguanafix.iguanafixdardotest.userinterface.fragments.ContactListFragment;
import com.iguanafix.iguanafixdardotest.userinterface.interfaces.TextWatcherAfterTextChanged;
import com.iguanafix.iguanafixdardotest.userinterface.viewmodels.ContactListActivityViewModel;

import butterknife.BindView;
import butterknife.ButterKnife;

import static android.content.res.Configuration.ORIENTATION_LANDSCAPE;

public class ContactListActivity extends AppCompatActivity implements ContactListFragment.OnContactSelectedListener {

    private static final String CONTACT_DETAILS_FRAGMENT_TAG = "contactDetailsFragment";

    private MenuItem searchMenuItem;
    private ActionBar actionBar;
    private EditText searchEditText;
    private boolean isSearchOpened = false;
    private ContactDetailsFragment contactDetailsFragment;
    private ContactListFragment contactListFragment;

    @BindView(R.id.toolbar) Toolbar toolbar;
    @BindView(R.id.contactDetailContainerLinearLayout) LinearLayout contactDetailContainerLinearLayout;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_contact_list);
        ButterKnife.bind(this);

        setupActionBar();
        setupFragments();
    }

    private void setupActionBar() {
        setSupportActionBar(toolbar);
        actionBar = getSupportActionBar();
        actionBar.setTitle(R.string.contact_list_activity_action_bar_title);
        actionBar.setCustomView(R.layout.search_bar);
        searchEditText = actionBar.getCustomView().findViewById(R.id.searchEditText);
        searchEditText.setOnEditorActionListener(searchEditTextOnEditorActionListener);
        searchEditText.addTextChangedListener(searchEditTextTextChangedListener);
    }

    private void setupFragments() {
        contactListFragment = (ContactListFragment) getSupportFragmentManager().findFragmentById(R.id.contact_list_fragment);
        processContactDetailsFragment();
    }

    private void processContactDetailsFragment() {
        boolean isTabletLandscape = getResources().getBoolean(R.bool.is_tablet) && getResources().getConfiguration().orientation == ORIENTATION_LANDSCAPE;

        // Si la instancia se recreó a través del savedInstanceState puede existir un ContactDetailsFragment
        contactDetailsFragment = (ContactDetailsFragment) getSupportFragmentManager().findFragmentByTag(CONTACT_DETAILS_FRAGMENT_TAG);
        if(contactDetailsFragment != null) {
            // La instancia efectivamente fue recreada a través del savedInstanceState
            if(!isTabletLandscape) {
                // Si la pantalla está en portrait elimino el fragment ya que no se lo deberia ver
                contactDetailContainerLinearLayout.setVisibility(View.GONE);
                getSupportFragmentManager().beginTransaction().remove(contactDetailsFragment).commit();
                ContactListActivityViewModel viewModel = ViewModelProviders.of(this).get(ContactListActivityViewModel.class);
                viewModel.clearContactDetail();
                contactDetailsFragment = null;
            }
            return;
        }

        // No se recreó ningún ContactDetailsFragment a través del savedInstanceState, ver si hay que crearlo
        if (isTabletLandscape) {
            contactDetailContainerLinearLayout.setVisibility(View.VISIBLE);
            contactDetailsFragment = new ContactDetailsFragment();
            getSupportFragmentManager().beginTransaction().add(R.id.contactDetailFragmentLinearLayout, contactDetailsFragment, CONTACT_DETAILS_FRAGMENT_TAG).commit();
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onPrepareOptionsMenu(Menu menu) {
        searchMenuItem = menu.findItem(R.id.action_search);
        return super.onPrepareOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();

        switch (id) {
            case R.id.action_search:
                handleMenuSearch();
                return true;
        }

        return super.onOptionsItemSelected(item);
    }

    protected void handleMenuSearch() {
        if(isSearchOpened) {
            hideSearchComponent();
        } else {
            showSearchComponent();
        }
    }

    private void showSearchComponent() {
        actionBar.setDisplayShowCustomEnabled(true);
        actionBar.setDisplayShowTitleEnabled(false);
        searchMenuItem.setIcon(getResources().getDrawable(android.R.drawable.ic_menu_close_clear_cancel));
        searchEditText.requestFocus();
        showKeyboard();
        isSearchOpened = true;
    }

    private void hideSearchComponent() {
        hideKeyboard();
        actionBar.setDisplayShowCustomEnabled(false);
        actionBar.setDisplayShowTitleEnabled(true);
        searchMenuItem.setIcon(getResources().getDrawable(android.R.drawable.ic_menu_search));
        isSearchOpened = false;
        clearSearch();
    }

    private void showKeyboard() {
        InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
        imm.showSoftInput(searchEditText, InputMethodManager.SHOW_IMPLICIT);
    }

    private void hideKeyboard() {
        InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
        imm.hideSoftInputFromWindow(searchEditText.getWindowToken(), 0);
    }

    private TextView.OnEditorActionListener searchEditTextOnEditorActionListener = new TextView.OnEditorActionListener() {
        @Override
        public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
            if (actionId == EditorInfo.IME_ACTION_SEARCH) {
                doSearch(searchEditText.getText().toString());
                return true;
            }
            return false;
        }
    };

    private TextWatcherAfterTextChanged searchEditTextTextChangedListener = new TextWatcherAfterTextChanged() {
        @Override
        public void afterTextChanged(Editable s) {
            doSearch(searchEditText.getText().toString());
        }
    };

    private void doSearch(String what) {
        contactListFragment.doSearch(what);
    }

    private void clearSearch() {
        contactListFragment.clearSearch();
    }

    @Override
    public void OnContactSelected(ContactDetail contactDetail) {
        if(contactDetailsFragment == null) {
            Intent intent = ContactDetailsActivity.createStartIntent(this, contactDetail);
            startActivity(intent);
        }
    }

    @Override
    public void onBackPressed() {
        if(isSearchOpened) {
            hideSearchComponent();
            return;
        }
        super.onBackPressed();
    }
}
