package com.iguanafix.iguanafixdardotest.models;

import android.os.Parcel;
import android.os.Parcelable;

public class Phone implements Parcelable {
    public enum Types {
        Home("Home"),
        Cellphone("Cellphone"),
        Office("Office");

        private String type;

        Types(String type) {
            this.type = type;
        }

        public String getValue() {
            return type;
        }
    }

    private String type;
    private String number;

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getNumber() {
        return number;
    }

    public void setNumber(String number) {
        this.number = number;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(this.type);
        dest.writeString(this.number);
    }

    public Phone() {
    }

    protected Phone(Parcel in) {
        this.type = in.readString();
        this.number = in.readString();
    }

    public static final Creator<Phone> CREATOR = new Creator<Phone>() {
        @Override
        public Phone createFromParcel(Parcel source) {
            return new Phone(source);
        }

        @Override
        public Phone[] newArray(int size) {
            return new Phone[size];
        }
    };
}