package com.iguanafix.iguanafixdardotest.models;

import android.os.Parcel;
import android.os.Parcelable;

import java.util.ArrayList;


public class ContactDetail implements Parcelable {

    private int id;
    private String userId;
    private String createdAt;
    private String birthDate;
    private String firstName;
    private String lastName;
    private String thumb;
    private String photo;
    private ArrayList<Phone> phones = new ArrayList<>();
    private ArrayList<String> homeAddresses = new ArrayList<>();
    private ArrayList<String> workAddresses = new ArrayList<>();

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public String getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(String createdAt) {
        this.createdAt = createdAt;
    }

    public String getBirthDate() {
        return birthDate;
    }

    public void setBirthDate(String birthDate) {
        this.birthDate = birthDate;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public ArrayList<Phone> getPhones() {
        return phones;
    }

    public void setPhones(ArrayList<Phone> phones) {
        this.phones = phones;
    }

    public String getThumb() {
        return thumb;
    }

    public void setThumb(String thumb) {
        this.thumb = thumb;
    }

    public String getPhoto() {
        return photo;
    }

    public void setPhoto(String photo) {
        this.photo = photo;
    }

    public ArrayList<String> getHomeAddresses() {
        return homeAddresses;
    }

    public void setHomeAddresses(ArrayList<String> homeAddresses) {
        this.homeAddresses = homeAddresses;
    }

    public ArrayList<String> getWorkAddresses() {
        return workAddresses;
    }

    public void setWorkAddresses(ArrayList<String> workAddresses) {
        this.workAddresses = workAddresses;
    }

    public Phone getPhoneByType(Phone.Types type) {
        for(Phone phone : phones) {
            if (type.getValue().equals(phone.getType())) {
                return phone;
            }
        }
        return null;
    }

    public ContactDetail() {
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeInt(this.id);
        dest.writeString(this.userId);
        dest.writeString(this.createdAt);
        dest.writeString(this.birthDate);
        dest.writeString(this.firstName);
        dest.writeString(this.lastName);
        dest.writeString(this.thumb);
        dest.writeString(this.photo);
        dest.writeTypedList(this.phones);
        dest.writeStringList(this.homeAddresses);
        dest.writeStringList(this.workAddresses);
    }

    protected ContactDetail(Parcel in) {
        this.id = in.readInt();
        this.userId = in.readString();
        this.createdAt = in.readString();
        this.birthDate = in.readString();
        this.firstName = in.readString();
        this.lastName = in.readString();
        this.thumb = in.readString();
        this.photo = in.readString();
        this.phones = in.createTypedArrayList(Phone.CREATOR);
        this.homeAddresses = in.createStringArrayList();
        this.workAddresses = in.createStringArrayList();
    }

    public static final Creator<ContactDetail> CREATOR = new Creator<ContactDetail>() {
        @Override
        public ContactDetail createFromParcel(Parcel source) {
            return new ContactDetail(source);
        }

        @Override
        public ContactDetail[] newArray(int size) {
            return new ContactDetail[size];
        }
    };
}