package com.iguanafix.iguanafixdardotest.helpers;

import android.util.Log;

public class LogHelper {

    private static String tag = "IguanaFix-DardoTest";

    /***
     * Flag para configurar automáticamente si se habilita el log o no
     * dependiendo del flavor que se este usando en esta compilacion
     * segun los definidos en el archivo de gradle.
     * Por ejemplo
     *      environment development - compilation debug => se habilita
     *      environment development - compilation release => se habilita
     *      environment production - compilation debug => se habilita
     *      environment production - compilation release (para enviar al store) => NO se habilita
     * Para este test lo dejo hardcodeado en true
     */
    private static final boolean enableLogging = true;

    public static void debug(Object from, String data)	{
        if(enableLogging) {
            String log = data == null ? "null" : data;
            Log.d(tag, from.getClass().getSimpleName() + ": " + log);
        }
    }

    public static void exception(Exception ex)	{
        if(enableLogging) {
            ex.printStackTrace();
        }
    }
}
