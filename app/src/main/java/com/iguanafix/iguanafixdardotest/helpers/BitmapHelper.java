package com.iguanafix.iguanafixdardotest.helpers;

import android.graphics.Bitmap;

import java.io.ByteArrayOutputStream;

public class BitmapHelper {

    public static Bitmap resizeAndCompress(int photoSize, Bitmap bitmap) {
        Bitmap resized = resize(photoSize, bitmap);
        return compress(resized);
    }

    public static Bitmap compress(Bitmap bitmap) {
        Bitmap compressedBitmap = Bitmap.createBitmap(bitmap, 0, 0, bitmap.getWidth(), bitmap.getHeight());
        ByteArrayOutputStream stream = new ByteArrayOutputStream();
        compressedBitmap.compress(Bitmap.CompressFormat.JPEG, 100, stream);
        return compressedBitmap;
    }

    public static Bitmap resize(int photoSize, Bitmap bitmap) {
        float aspectRatio = (float)bitmap.getWidth() / (float)bitmap.getHeight();
        LogHelper.debug(BitmapHelper.class,"Original bitmap: width=" + bitmap.getWidth() + " - height=" + bitmap.getHeight() + " - aspect ratio=" + aspectRatio);

        Bitmap resizedBitmap;
        if (aspectRatio >= 1) {
            resizedBitmap = Bitmap.createScaledBitmap(bitmap, photoSize, (int) (photoSize / aspectRatio), false);
        } else {
            resizedBitmap = Bitmap.createScaledBitmap(bitmap, (int) (photoSize * aspectRatio), photoSize, false);
        }
        LogHelper.debug(BitmapHelper.class,"Resized bitmap: width=" + resizedBitmap.getWidth() + " - height=" + resizedBitmap.getHeight());
        return resizedBitmap;
    }
}
