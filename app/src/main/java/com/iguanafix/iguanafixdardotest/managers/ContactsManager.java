package com.iguanafix.iguanafixdardotest.managers;

import android.content.Context;
import com.iguanafix.iguanafixdardotest.IguanaFixDardoTestApplication;
import com.iguanafix.iguanafixdardotest.models.ContactDetail;
import com.iguanafix.iguanafixdardotest.rest.backends.IguanaFixTestBackend;
import com.iguanafix.iguanafixdardotest.rest.support.DisposableRequest;
import com.iguanafix.iguanafixdardotest.rest.support.OnItemRequestCompleted;
import com.iguanafix.iguanafixdardotest.rest.support.OnListRequestCompleted;

import java.util.ArrayList;

public class ContactsManager {

    private static ContactsManager contactsManager;
    private Context context;
    private IguanaFixTestBackend iguanaFixTestBackend;

    private ContactsManager(Context context) {
        this.context = context;
        iguanaFixTestBackend = IguanaFixTestBackend.getInstance();
    }

    public static ContactsManager getInstance() {
        if(contactsManager == null) {
            contactsManager = new ContactsManager(IguanaFixDardoTestApplication.getAppContext());
        }
        return contactsManager;
    }

    public DisposableRequest getContacts(final OnListRequestCompleted<ContactDetail> onRequestCompleted) {
        return iguanaFixTestBackend.getContacts(new OnListRequestCompleted<ContactDetail>() {
            @Override
            public void onSuccess(ArrayList<ContactDetail> contactDetails) {
                int id = 1;
                for (ContactDetail contactDetail : contactDetails) {
                    contactDetail.setId(id++);
                }
                onRequestCompleted.onSuccess(contactDetails);
            }

            @Override
            public void onError(Throwable throwable) {
                onRequestCompleted.onError(throwable);
            }
        });
    }

    public DisposableRequest getContact(String contactId, OnItemRequestCompleted<ContactDetail> onRequestCompleted) {
        return iguanaFixTestBackend.getContact(contactId, onRequestCompleted);
    }
}
