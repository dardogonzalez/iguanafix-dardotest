package com.iguanafix.iguanafixdardotest.rest.backendmodels;

import com.google.gson.annotations.SerializedName;
import com.iguanafix.iguanafixdardotest.models.ContactDetail;

import java.util.ArrayList;
import java.util.List;

public class JsonBackendContactDetail extends JsonBackendModelOf<ContactDetail> {
    @SerializedName("user_id") private String userId;
    @SerializedName("created_at") private String createdAt;
    @SerializedName("birth_date") private String birthDate;
    @SerializedName("first_name") private String firstName;
    @SerializedName("last_name") private String lastName;
    @SerializedName("thumb") private String thumb;
    @SerializedName("photo") private String photo;
    @SerializedName("phones") private List<JsonBackendPhone> jsonBackendPhones = new ArrayList<>();
    @SerializedName("addresses") private List<JsonBackendAddress> jsonBackendAddresses = new ArrayList<>();

    @Override
    public ContactDetail getLocalModel() {
        ContactDetail contactDetail = new ContactDetail();
        contactDetail.setUserId(userId);
        contactDetail.setCreatedAt(createdAt);
        contactDetail.setBirthDate(birthDate);
        contactDetail.setFirstName(firstName);
        contactDetail.setLastName(lastName);
        contactDetail.setThumb(thumb);
        contactDetail.setPhoto(photo);
        for (JsonBackendPhone jsonBackendPhone : jsonBackendPhones) {
            contactDetail.getPhones().add(jsonBackendPhone.getLocalModel());
        }

        /**
         * El model del backend tiene arrays de direcciones con un home y work
         * address en cada item, ese diseño es muuuuy extraño.
         * A modo de ejemplo de desacople para esta prueba, en el
         * model local no va a existir ese array, y los valores
         * que llegan del server se van a mapear en dos arraylists de strings,
         * uno con las direcciones de home y otro con las de work
         */
        for (JsonBackendAddress jsonBackendAddress : jsonBackendAddresses) {
            if(jsonBackendAddress.getHome() != null) {
                contactDetail.getHomeAddresses().add(jsonBackendAddress.getHome());
            }
            if(jsonBackendAddress.getWork() != null) {
                contactDetail.getWorkAddresses().add(jsonBackendAddress.getWork());
            }
        }

        return contactDetail;
    }
}
