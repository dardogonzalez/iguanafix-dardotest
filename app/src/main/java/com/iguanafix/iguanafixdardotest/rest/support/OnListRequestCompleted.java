package com.iguanafix.iguanafixdardotest.rest.support;

import java.util.ArrayList;

public interface OnListRequestCompleted<T> {
    void onSuccess(ArrayList<T> t);
    void onError(Throwable throwable);
}
