package com.iguanafix.iguanafixdardotest.rest.backends;

import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

/**
 * Esta clase se debe implementar para cada backend
 * con el que la app esté interactuando
 */
public abstract class BaseBackend<T> {

    protected T api;

    abstract String getBackendDomain();
    abstract Class<T> apiClass();

    public BaseBackend() {
        api = configureRetrofit();
    }

    private T configureRetrofit() {
        return (T) new Retrofit.Builder()
                .baseUrl(getBackendDomain())
                .addConverterFactory(GsonConverterFactory.create())
                .build().create(apiClass());
    }
}
