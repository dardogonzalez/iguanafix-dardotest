package com.iguanafix.iguanafixdardotest.rest.support;

public interface OnItemRequestCompleted<T> {
    void onSuccess(T t);
    void onError(Throwable throwable);
}
