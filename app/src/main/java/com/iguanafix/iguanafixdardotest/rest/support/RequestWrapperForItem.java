package com.iguanafix.iguanafixdardotest.rest.support;

import com.iguanafix.iguanafixdardotest.rest.backendmodels.JsonBackendModelOf;

import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class RequestWrapperForItem<T, S extends JsonBackendModelOf<T>> implements Callback<S>, DisposableRequest {

    private final Call<S> call;
    private final OnItemRequestCompleted<T> onRequestCompleted;

    // Si se pidió la cancelacion no se ejecuta el callback, independientemente
    // de si la llamada al backend fue efectivamente cancelada o no
    private boolean cancelSignalled;

    public RequestWrapperForItem(Call<S> call, OnItemRequestCompleted<T> onRequestCompleted) {
        this.call = call;
        this.onRequestCompleted = onRequestCompleted;
    }

    @Override
    public void dispose() {
        cancelSignalled = true;
        call.cancel();
    }

    public void executeAsync() {
        call.enqueue(this);
    }

    @Override
    public void onResponse(Call<S> call, Response<S> response) {
        if (cancelSignalled) {
            return;
        }
        if (response.isSuccessful()) {
            onRequestCompleted.onSuccess(response.body().getLocalModel());
        } else {
            /**
             * Aquí se deberían administrar los posibles errores de red.
             * Para esta prueba solamente copio el error code.
             */
            onRequestCompleted.onError(new Throwable(String.valueOf(response.code())));
        }
    }

    @Override
    public void onFailure(Call<S> call, Throwable t) {
        if(!cancelSignalled) {
            onRequestCompleted.onError(t);
        }
    }
}
