package com.iguanafix.iguanafixdardotest.rest.backends;

import com.iguanafix.iguanafixdardotest.rest.backendmodels.JsonBackendContactDetail;

import java.util.List;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Path;

interface IguanaFixTestBackendAPIs {

    @GET("contacts")
    Call<List<JsonBackendContactDetail>> getContactList();

    @GET("contacts/{id}")
    Call<JsonBackendContactDetail> getContact(@Path("id") String contactId);
}
