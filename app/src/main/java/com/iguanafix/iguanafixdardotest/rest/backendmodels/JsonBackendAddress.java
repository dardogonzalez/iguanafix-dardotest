package com.iguanafix.iguanafixdardotest.rest.backendmodels;

import com.google.gson.annotations.SerializedName;

public class JsonBackendAddress extends JsonBackendModelOf<Void> {

    @SerializedName("work") private String work;
    @SerializedName("home") private String home;

    public String getWork() {
        return work;
    }

    public String getHome() {
        return home;
    }

    /**
     * Como el address no lo manejamos como un model localmente
     * sino como un atributo de ContactDetail devuelve null
     */
    @Override
    public Void getLocalModel() {
        return null;
    }
}
