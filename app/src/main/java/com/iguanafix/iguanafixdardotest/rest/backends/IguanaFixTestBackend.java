package com.iguanafix.iguanafixdardotest.rest.backends;

import com.iguanafix.iguanafixdardotest.models.ContactDetail;
import com.iguanafix.iguanafixdardotest.rest.support.DisposableRequest;
import com.iguanafix.iguanafixdardotest.rest.support.OnItemRequestCompleted;
import com.iguanafix.iguanafixdardotest.rest.support.OnListRequestCompleted;
import com.iguanafix.iguanafixdardotest.rest.support.RequestWrapperForItem;
import com.iguanafix.iguanafixdardotest.rest.support.RequestWrapperForList;

public class IguanaFixTestBackend extends BaseBackend<IguanaFixTestBackendAPIs> {

    private static IguanaFixTestBackend iguanaFixTestBackend;

    private IguanaFixTestBackend() {
        super();
    }

    public static IguanaFixTestBackend getInstance() {
        if(iguanaFixTestBackend == null) {
            iguanaFixTestBackend = new IguanaFixTestBackend();
        }
        return iguanaFixTestBackend;
    }

    @Override
    String getBackendDomain() {
        /***
         * Aquí se debería configurar la url del backend dependiendo
         * del flavor que se este usando en esta compilacion
         * segun los definidos en el archivo de gradle.
         * Por ejemplo
         *      environment development - "https://public-apis.dev.iguanafix"
         *      environment production - "https://public-apis.iguanafix"
         * Para este test lo dejo hardcodeado con la url de prueba
         */
        return "https://private-d0cc1-iguanafixtest.apiary-mock.com/";
    }

    @Override
    Class<IguanaFixTestBackendAPIs> apiClass() {
        return IguanaFixTestBackendAPIs.class;
    }


    public DisposableRequest getContacts(OnListRequestCompleted<ContactDetail> onRequestCompleted) {
        RequestWrapperForList requestWrapper = new RequestWrapperForList<>(api.getContactList(), onRequestCompleted);
        requestWrapper.executeAsync();
        return requestWrapper;
    }

    public DisposableRequest getContact(String contactId, OnItemRequestCompleted<ContactDetail> onRequestCompleted) {
        RequestWrapperForItem requestWrapper = new RequestWrapperForItem<>(api.getContact(contactId), onRequestCompleted);
        requestWrapper.executeAsync();
        return requestWrapper;
    }

}








