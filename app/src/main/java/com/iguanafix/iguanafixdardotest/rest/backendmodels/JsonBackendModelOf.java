package com.iguanafix.iguanafixdardotest.rest.backendmodels;

/**
 * Esta clase se debe implementar para mapear los datos tal cual
 * los devuelve cada endpoint al que se le esté pegando.
 * El método getLocalModel debe devolver un objeto que representa el model que usa
 * la app, de manera de desacoplar el modelo de datos de los backends y el
 * modelo de datos definidos localmente
 */
public abstract class JsonBackendModelOf<T> {
    public abstract T getLocalModel();
}
