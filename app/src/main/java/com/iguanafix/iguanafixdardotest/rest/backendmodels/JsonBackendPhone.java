package com.iguanafix.iguanafixdardotest.rest.backendmodels;

import com.google.gson.annotations.SerializedName;
import com.iguanafix.iguanafixdardotest.models.Phone;

public class JsonBackendPhone extends JsonBackendModelOf<Phone> {
    @SerializedName("type") private String type;
    @SerializedName("number") private String number;

    @Override
    public Phone getLocalModel() {
        Phone phone = new Phone();
        phone.setNumber(number);
        phone.setType(type);
        return phone;
    }
}
